<?php

//create admin
$router->get("admin/createAdmin","AdminController@createAdmin");

$router->get("","HomepageController@index");

//sign in
$router->get("signin","HomepageController@signin");
$router->post("processLogin","HomepageController@processLogin");

//sign up
$router->get("signup","HomepageController@signup");
$router->post("processSignup","HomepageController@processSignup");

//sign out
$router->get("signout","HomepageController@signout");



//articles
$router->get("articles","ArticlesController@index");
$router->get("article/add","ArticlesController@add");
$router->post("article/process","ArticlesController@processForm");
$router->get("article/view","ArticlesController@view");

//comments
$router->post("article/processComment","ArticlesController@processComment");

//tag
$router->get("tag","ArticlesController@tag");

//user`s articles
$router->get("articles/user","ArticlesController@user");



