<?php

class TagsManager {

    /**
     * process tags from form written in tag1, tag2, tag3, ...
     */
    public function processTags($tags, $article) {

        $imploded = explode(",", $tags);

        foreach ($imploded as $tag) {
            $processedTag = strtolower(trim($tag));

            $searchedTag = App::get('em')->getRepository(\Models\Entities\Tag::class)->findOneBy(['name' => $processedTag]);

            if (!$searchedTag) {
                $tag = new \Models\Entities\Tag();
                $tag->setName($processedTag);
            } else {
                $tag = $searchedTag;
            }

            $tag->addArticle($article);
        }

        App::get('em')->persist($tag);
        App::get('em')->flush();
    }

}
