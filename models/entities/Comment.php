<?php

namespace Models\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Comment {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * Many comments have one article.
     * @ORM\ManyToOne(targetEntity="Article", inversedBy="comments")
     * @ORM\JoinColumn(name="article_id", referencedColumnName="id")
     */
    private $article;

    /**
     * @ORM\Column(type="text")
     */
    private $comment;

    /**
     * @ORM\Column(type="text")
     */
    private $name;

    /**
     * @ORM\Column(type="datetime")
     */
    private $time;

    public function __construct() {
        $this->time = new \Datetime;
    }

    public function getId() {
        return $this->id;
    }

    public function setArticle(Article $article) {
        $this->article = $article;
    }

    public function setText($text) {
        $this->comment = $text;
    }

    public function getText() {
        return $this->comment;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function getName() {
        return $this->name;
    }

    public function setTime() {
        return new Datetime;
    }

    public function getTime() {
        return $this->time;
    }

}
