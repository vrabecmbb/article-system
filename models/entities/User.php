<?php

namespace Models\Entities;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 */
class User {

    public function __construct() {
        $this->articles = new ArrayCollection();
    }

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $role;

    /**
     * @ORM\Column(type="string")
     */
    private $login;

    /**
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * One User has Many Articles.
     * @ORM\OneToMany(targetEntity="Article", mappedBy="user")
     */
    private $articles;

    function getId() {
        return $this->id;
    }

    function getRole() {
        return $this->role;
    }

    function getLogin() {
        return $this->login;
    }

    function getPassword() {
        return $this->password;
    }

    function getArticles() {
        return $this->articles;
    }

    function setRole($role) {
        $this->role = $role;
    }

    function setLogin($login) {
        $this->login = $login;
    }

    function setPassword($password) {
        $this->password = $password;
    }

    function setArticles($articles) {
        $this->articles = $articles;
    }

}
