<?php

namespace Models\Entities;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 */
class Tag {

    public function __construct() {
        $this->articles = new ArrayCollection;
    }

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @ORM\Column (type="string") 
     */
    private $name;

    /**
     * Many Tags have Many Articles.
     * @ORM\ManyToMany(targetEntity="Article", inversedBy="tags")
     * @ORM\JoinTable(name="tags_articles")
     */
    private $articles;

    function getId() {
        return $this->id;
    }

    function getName() {
        return $this->name;
    }

    function getArticles() {
        return $this->articles;
    }

    function setName($name) {
        $this->name = $name;
    }

    function addArticle($article) {
        $this->articles[] = $article;
    }

}
