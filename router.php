<?php

class Router{

    protected $routes = [
        'GET' => [],
        'POST' => []
    ];
    
    public static function load($file) {

        $router = new static;

        require $file;

        return $router;
    }

    public function get($uri, $controller)
    {
        $this->routes['GET'][$uri] = $controller;
    }

    public function post($uri, $controller)
    {
        $this->routes['POST'][$uri] = $controller;
    }

    public function direct($uri, $requestMethod){
        
        $exploded = explode("/", $uri);
        $id = is_numeric(end($exploded)) ? end($exploded) : NULL;
        
        if ($id){
            array_pop($exploded);
            $uri = implode("/", $exploded);
        }

        if (array_key_exists($uri, $this->routes[$requestMethod])) {
            $route = explode('@', $this->routes[$requestMethod][$uri]);
            $controller = $route[0];
            $action = $route[1];

            return $this->callAction($controller,$action,$id);
        }

        throw new Exception("No route defined for this URI ({$uri}).");
    }

    public function callAction($controller, $action, $id = NULL){

        $controller = "Controllers\\{$controller}";

        if (! method_exists($controller, $action)) {
            throw new Exception(
                "{$controller} does not have action called {$action}."
            );
        }

        $controller = new $controller;

        return $controller->$action($id);
    }
}