-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: blog
-- ------------------------------------------------------
-- Server version	5.7.22-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `article`
--

DROP TABLE IF EXISTS `article`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `perex` longtext COLLATE utf8_unicode_ci NOT NULL,
  `text` longtext COLLATE utf8_unicode_ci NOT NULL,
  `image` longtext COLLATE utf8_unicode_ci,
  `publicationDate` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_CD8737FAA76ED395` (`user_id`),
  CONSTRAINT `FK_CD8737FAA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `article`
--

LOCK TABLES `article` WRITE;
/*!40000 ALTER TABLE `article` DISABLE KEYS */;
INSERT INTO `article` VALUES (1,2,'Lorem Ipsum','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin luctus a leo nec dapibus. In feugiat erat sit amet augue molestie, ut vestibulum arcu maximus. Duis tincidunt enim vitae ornare mattis. Fusce eu sapien at leo commodo congue quis sed odio. Curabitur tincidunt arcu ut leo dignissim euismod. Nullam condimentum aliquam orci, ac vehicula lectus iaculis et. Praesent purus nunc, ornare at tempus ac, malesuada vel dui. Vestibulum a consequat augue.','\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Proin luctus a leo nec dapibus. In feugiat erat sit amet augue molestie, ut vestibulum arcu maximus. Duis tincidunt enim vitae ornare mattis. Fusce eu sapien at leo commodo congue quis sed odio. Curabitur tincidunt arcu ut leo dignissim euismod. Nullam condimentum aliquam orci, ac vehicula lectus iaculis et. Praesent purus nunc, ornare at tempus ac, malesuada vel dui. Vestibulum a consequat augue. Donec eget rutrum odio. In leo ante, fermentum sed mi ut, lobortis eleifend diam. Cras sit amet ipsum in velit tempus viverra. Nam tortor neque, cursus aliquam placerat sit amet, euismod sed arcu. Sed dictum ultricies elit ut accumsan. Duis tincidunt enim quis erat hendrerit, eu consequat ipsum dictum. Vivamus sodales, mauris id aliquam fermentum, tortor neque ultricies est, sit amet tincidunt sem risus at mi.\r\n\r\nUt accumsan a justo semper sodales. Ut vehicula ac magna vitae ultrices. Vivamus lacinia malesuada placerat. Donec eu sapien at ex facilisis consectetur. In diam ipsum, bibendum id diam cursus, consectetur condimentum quam. Aliquam sed imperdiet velit. Vivamus tristique, tortor eu convallis iaculis, dolor odio ultrices ligula, vitae congue dui urna at leo. Ut sed egestas magna. Mauris pharetra nec enim a placerat. Pellentesque eu orci ex. Nunc congue interdum nisl a suscipit.\r\n\r\nNunc mattis et lacus id lacinia. Curabitur elementum enim sed nisi ultrices lacinia. Integer accumsan tempus risus a congue. Vestibulum tempor porttitor volutpat. Aenean scelerisque neque vel tempor laoreet. Praesent vel arcu fermentum, molestie eros vel, malesuada odio. Nunc at mauris vitae mauris finibus pharetra id a felis. Mauris auctor tortor in scelerisque dignissim. Praesent rutrum mauris eu rutrum fringilla. Quisque ac mollis lorem.\r\n\r\nSuspendisse potenti. Ut pharetra metus at ultricies sodales. Etiam pulvinar, felis ut facilisis pellentesque, leo nisi accumsan est, et ultricies sapien enim et lectus. Nulla ornare dolor vel viverra consectetur. Nam semper odio orci, at aliquam enim fermentum quis. Vivamus in facilisis dui. Mauris non tortor sed tortor accumsan lobortis id venenatis justo. Mauris fringilla vitae tortor non lobortis. Sed nec mi ipsum. Fusce faucibus pretium massa eget tincidunt. Duis accumsan tincidunt lacus a varius. Ut at tellus ipsum. Aenean ullamcorper, lorem non euismod molestie, quam nulla fringilla mauris, nec semper nibh ligula non tortor. Mauris fringilla gravida rhoncus. In eu risus eget ligula interdum sollicitudin. Interdum et malesuada fames ac ante ipsum primis in faucibus.\r\n\r\nQuisque eget placerat nisi. Nulla id quam dui. Duis ut lacus et nisi vulputate blandit eget ut arcu. Ut ipsum justo, bibendum et diam sed, suscipit volutpat sapien. Aliquam erat volutpat. Quisque sodales volutpat dui. Proin mauris lectus, fermentum vel convallis tristique, scelerisque eu risus. Aliquam in scelerisque massa. Aliquam in congue arcu. Duis lacus eros, rutrum nec neque malesuada, accumsan tincidunt enim. Nam vestibulum blandit massa, at varius elit lacinia sed. In risus nisl, commodo vel luctus quis, semper in erat. Donec libero dui, aliquet eget vehicula vitae, volutpat ut sem. Donec varius tortor non ornare dictum. ','public/images/loremipsum.png','2019-08-20 10:34:38'),(2,3,'Jano pÃ­Å¡e','Jano pÃ­Å¡e testovacÃ­ ÄlÃ¡nok','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent lacinia ipsum dui, vestibulum tempus arcu rhoncus vitae. Sed eu massa faucibus, faucibus urna eu, pretium quam. Vestibulum maximus ut erat in pharetra. Integer feugiat sapien efficitur magna interdum dapibus. Suspendisse sed porta nulla. Integer sed est ipsum. In laoreet orci eros, ut mattis lorem cursus quis. Nunc nisl quam, efficitur eget sem et, scelerisque mollis libero. Duis pellentesque non augue eget consequat. Aenean semper blandit vehicula. Nulla ultrices eros sapien, vel facilisis enim vestibulum eu. Etiam libero sem, condimentum in sem sed, maximus auctor ipsum. Nullam dictum ex eu quam dapibus, ultrices tempus tortor porttitor. Quisque eleifend eros eget commodo vestibulum. ','public/images/loremipsum.png','2019-08-20 14:51:05');
/*!40000 ALTER TABLE `article` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comment`
--

DROP TABLE IF EXISTS `comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article_id` int(11) DEFAULT NULL,
  `comment` longtext COLLATE utf8_unicode_ci NOT NULL,
  `time` datetime NOT NULL,
  `name` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_5BC96BF07294869C` (`article_id`),
  CONSTRAINT `FK_5BC96BF07294869C` FOREIGN KEY (`article_id`) REFERENCES `article` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comment`
--

LOCK TABLES `comment` WRITE;
/*!40000 ALTER TABLE `comment` DISABLE KEYS */;
INSERT INTO `comment` VALUES (1,1,'Testovaci koment','2019-08-20 12:47:26','Test'),(2,1,'TestovacÃ­ komentÃ¡r 2','2019-08-20 14:56:40','Test2');
/*!40000 ALTER TABLE `comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tag`
--

DROP TABLE IF EXISTS `tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tag`
--

LOCK TABLES `tag` WRITE;
/*!40000 ALTER TABLE `tag` DISABLE KEYS */;
INSERT INTO `tag` VALUES (1,'lorem-ipsum');
/*!40000 ALTER TABLE `tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tags_articles`
--

DROP TABLE IF EXISTS `tags_articles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tags_articles` (
  `tag_id` int(11) NOT NULL,
  `article_id` int(11) NOT NULL,
  PRIMARY KEY (`tag_id`,`article_id`),
  KEY `IDX_D54BAD71BAD26311` (`tag_id`),
  KEY `IDX_D54BAD717294869C` (`article_id`),
  CONSTRAINT `FK_D54BAD717294869C` FOREIGN KEY (`article_id`) REFERENCES `article` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_D54BAD71BAD26311` FOREIGN KEY (`tag_id`) REFERENCES `tag` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tags_articles`
--

LOCK TABLES `tags_articles` WRITE;
/*!40000 ALTER TABLE `tags_articles` DISABLE KEYS */;
INSERT INTO `tags_articles` VALUES (1,1),(1,2);
/*!40000 ALTER TABLE `tags_articles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `login` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (2,'admin','admin@admin.sk','56b1db8133d9eb398aabd376f07bf8ab5fc584ea0b8bd6a1770200cb613ca005'),(3,'user','jano@jano.sk','9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-08-20 14:26:15
