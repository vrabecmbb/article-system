<?php

namespace Libs;

class FileUploder {

    private $file;
    private $extensions = [];

    /**
     * Set file to upload
     * @param type $file
     */
    function setFile($file) {

        $name = $file['name'];

        $tmp = explode('.', $name);
        $file_ext = end($tmp);

        if (!in_array($file_ext, $this->extensions)) {
            var_dump($file_ext, $this->extensions);
            throw new \Exception("File extension is not allowed");
        }

        $this->file = $file;
    }

    /**
     * Set possible extensions of file
     * @param array $extensions
     */
    function setExtensions(Array $extensions) {
        $this->extensions = $extensions;
    }

    /**
     * Upload file to defined storage
     * @param type $storage
     * @throws Exception
     */
    public function uploadFile($storage) {
        if (!$this->file) {
            throw new \Exception('File is not defined');
        }

        move_uploaded_file($this->file['tmp_name'], $storage . $this->file['name']);

        return $storage . $this->file['name'];
    }

}
