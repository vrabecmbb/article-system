<?php

namespace Controllers;

use App;
use Models\Entities\Article;
use Models\Entities\Comment;
use Models\Entities\Tag;
use Models\Entities\User;

class ArticlesController {

    public function index() {

        $date = new \DateTime;

        $queryBuilder = App::get('em')->createQueryBuilder();

        $queryBuilder
                ->select('a')
                ->from(Article::class, 'a')
                ->where('a.publicationDate <= :date')
                ->orderBy('a.publicationDate', 'DESC')
                ->setParameter('date', $date)
        ;

        $articles = $queryBuilder->getQuery()->getResult();

        $tags = App::get('em')->getRepository(Tag::class)->findAll();

        $data = [
            'articles' => $articles,
            'title' => 'Články',
        ];

        return view('articles.index', $data);
    }

    public function add() {
        if (!isset($_SESSION['loged_user'])) {
            return redirect('articles');
        }

        /*if ($_SESSION['loged_user']['role'] != 'admin') {
            return redirect('articles');
        }*/

        return view('article.form', ['title' => 'Pridanie článku', 'user' => $_SESSION['loged_user']['id']]);
    }

    public function view($id) {

        $article = App::get('em')->find(Article::class, $id);

        if (!$article) {
            return redirect('articles');
        }

        return view('article.view', [
            'title' => $article->getTitle(),
            'comments' => $article->getComments(),
            'article' => $article
                ]
        );
    }

    /**
     * View articles of tag
     *
     * @param [type] $id
     * @return void
     */
    public function tag($id) {

        $tag = App::get('em')->find(Tag::class, $id);

        if (!$tag) {
            return redirect('articles');
        }

        return view('article.tag', [
            'title' => "#{$tag->getName()}",
            'tag' => $tag,
            'articles' => $tag->getArticles()
            ]
        );
    }

    /**
     * View articles of user
     *
     * @param [type] $id
     * @return void
     */
    public function user($id) {

        $user = App::get('em')->find(User::class, $id);

        if (!$user) {
            return redirect('articles');
        }


        return view('article.user', [
            'title' => "{$user->getLogin()}",
            'user' => $user,
            'articles' => $user->getArticles()
                ]
        );
    }

    /**
     * Process adding article
     *
     * @return void
     */
    public function processForm() {
        $title = $_POST['title'];
        $perex = $_POST['perex'];
        $text = $_POST['text'];
        $tags = $_POST['tags'];
        $image = $_FILES['image'];
        $publicationDate = $_POST['publication_date'];
        $user = $_POST['user'];

        if ($image) {
            $fileUploader = new \Libs\FileUploder();
            $fileUploader->setExtensions(["jpg", "gif", "png"]);
            $fileUploader->setFile($image);
            $image = $fileUploader->uploadFile("public/images/");
        }

        App::get('em')->getConnection()->beginTransaction();

        try {
            $article = new \Models\Entities\Article;
            $article->setTitle($title);
            $article->setPerex($perex);
            $date = \DateTime::createFromFormat("Y-m-d", $publicationDate);
            $article->setPublicationDate($date);
            $article->setText($text);
            $article->setUser(App::get('em')->find(\Models\Entities\User::class, $user));
            $article->setImage(isset($image) ? $image : NULL );

            App::get('em')->persist($article);
            App::get('em')->flush();

            $tagsManager = new \TagsManager();
            $tagsManager->processTags($tags, $article);

            App::get('em')->getConnection()->commit();
        } catch (\Exception $e) {
            App::get('em')->getConnection()->rollBack();
            printf("Error: %s", $e->getMessage());
        }
    }

    /**
     * Process adding comment
     *
     * @return void
     */
    public function processComment() {
        $name = $_POST['name'];
        $text = $_POST['text'];
        $article = $_POST['article'];

        $findArticle = App::get('em')->find(Article::class, $article);

        if (empty($name) || empty($text)) {
            return redirect("article/view/{$article}");
        }

        $comment = new Comment;
        $comment->setName($name);
        $comment->setText($text);
        $comment->setArticle($findArticle);

        App::get('em')->persist($comment);
        App::get('em')->flush();

        return redirect('article/view/' . $article);
    }

}
