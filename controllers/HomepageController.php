<?php

namespace Controllers;

use App;
use Models\Entities\Article;
use Models\Entities\Tag;
use Models\Entities\User;

class HomepageController {

    public function index() {

        $date = new \DateTime;

        $queryBuilder = App::get('em')->createQueryBuilder();

        $queryBuilder
                ->select('a')
                ->from(Article::class, 'a')
                ->where('a.publicationDate <= :date')
                ->orderBy('a.publicationDate', 'DESC')
                ->setParameter('date', $date)
                ->setMaxResults(10)
        ;

        $articles = $queryBuilder->getQuery()->getResult();

        $tags = App::get('em')->getRepository(Tag::class)->findAll();

        $data = [
            'articles' => $articles,
            'title' => 'Hlavná stránka',
            'tags' => $tags
        ];

        return view('index', $data);
    }

    /**
     * Login user
     *
     * @return void
     */
    public function signin() {
        if (empty($_SESSION['loged_user'])) {
            return view('login.form', ['title' => 'Prihlásenie','action' => 'processLogin', 'submitValue' => 'Prihlásiť', 'formTitle' => 'Prihlásiť']);
        } else {
            redirect('');
        }
    }

    /**
     * Signup user
     *
     * @return void
     */
    public function signup() {
        return view('login.form', ['title' => 'Registrácia','action' => 'processSignup', 'submitValue' => 'Registrovať', 'formTitle' => 'Registrácia']);
    }

    /**
     * logout user
     *
     * @return void
     */
    public function signout() {
        if (!empty($_SESSION['loged_user'])) {
            unset($_SESSION['loged_user']);
        }
        redirect('');
    }

    /**
     * Signup process
     *
     * @return void
     */
    public function processSignup(){
        $email = $_POST['email'];
        $password = $_POST['password'];

        if (empty($email) || empty($password)){
            return view('login.form', ['title' => 'Registrácia','action' => 'processSignup', 'submitValue' => 'Registrovať', 'formTitle' => 'Registrácia', 'error' => 'Údaje neboli vyplnené']);
        }

        $findUser = App::get('em')->getRepository(User::class)->findOneBy(['login' => $email]); 

        if ($findUser){
            return view('login.form', ['title' => 'Registrácia','action' => 'processSignup', 'submitValue' => 'Registrovať', 'formTitle' => 'Registrácia', 'error' => 'Užívateľ už existuje']);
        }

        $user = new User;
        $user->setLogin($email);
        $user->setPassword(hash('sha256',$password));
        $user->setRole('user');

        App::get('em')->persist($user);
        App::get('em')->flush();

        return redirect("");

    }

    /**
     * Login process
     *
     * @return void
     */
    public function processLogin() {
        $email = $_POST['email'];
        $password = $_POST['password'];

        $findUser = App::get('em')->getRepository(User::class)->findOneBy(['login' => $email, 'password' => hash('sha256', $password)]);

        if ($findUser) {

            $store = [
                'email' => $findUser->getLogin(),
                'role' => $findUser->getRole(),
                'id' => $findUser->getId()
            ];

            $_SESSION['loged_user'] = $store;

            redirect('');
        } else {
            return view('login.form', ['Prihlásenie','action' => 'processLogin', 'submitValue' => 'Prihlásiť', 'formTitle' => 'Prihlásenie','error' => 'Užívateľ nenájdený']);
        }
    }

}
