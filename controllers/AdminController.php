<?php

namespace Controllers;

use App;

class AdminController {

    public function createAdmin() {
        $em = App::get('em');

        $admin = new \Models\Entities\User;
        $admin->setRole('admin');
        $admin->setLogin('admin@admin.sk');
        $admin->setPassword(hash('sha256', 'heslo'));

        $em->persist($admin);
        $em->flush();
    }

}
