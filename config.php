<?php

define('APP_HOMEPAGE', 'blog');
define('HOMEPAGE', 'http://localhost/blog');

$database = [
    'dbname' => 'blog',
    'user' => 'root',
    'password' => 'password',
    'host' => 'localhost',
    'driver' => 'pdo_mysql',
];