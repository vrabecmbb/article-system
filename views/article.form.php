<?php require 'views/include/header.php' ?>

<div class="col-md-12">
    <div class="box">
        <form action="process" method="POST" enctype="multipart/form-data">
            <?php
            if (isset($error)) {
                echo sprintf('<div class="alert alert-danger" role="alert">%s</div>', $error);
            }
            ?>
            <div class="form-group">
                <label for="title">Nadpis:</label>
                <input class="form-control" type="text" name="title">
            </div>
            <div class="form-group">
                <label for="title">Perex:</label>
                <textarea class="form-control" name="perex"></textarea>
            </div>
            <div class="form-group">
                <label for="title">Text:</label>
                <textarea id="text-textarea" class="form-control" name="text"></textarea>
            </div>
            <div class="form-group">
                <label for="title">Tagy (oddeliť čiarkou):</label>
                <input class="form-control" type="text" name="tags">
            </div>
            <div class="form-grooup">
                <label for="title">Čas zverejnenia:</label>
                <input class="form-control" type="date" name="publication_date">
            </div>
            <div class="form-grooup">
                <label for="title">Obrázok:</label>
                <input class="form-group" type="file" name="image">
            </div>
            <input type="hidden" name="user" value="<?= $user ?>">
            <div class="form-group">
                <button class="btn btn-primary" type="submit">Odoslať</button>
            </div>
        </form>    
    </div>
</div>
