<?php require 'views/include/header.php' ?>

<div class="col-md-12">
    <section id="articles">
        <div class="item">
            <div class="head">
                <h2><?= $article->getTitle() ?></h2>
                <div class="article_info">
                    <span><?= $article->getPublicationDate()->format('d/m/Y') ?></span>
                    <span><i class="far fa-user"></i> <?= $article->getUser()->getLogin() ?></span>
                </div>  
            </div>
            <div class="content">
                <img class="float-left" src="<?= HOMEPAGE . "/" . $article->getImage() ?>" />
                <p><?= $article->getPerex() ?></p>
                <div style="clear: both"></div>
                <p><?= $article->getText() ?></p>
            </div>
            <div style="clear: both"></div>
            <div class="footer">
                <div class="article_info">
                    <span><i class="far fa-comments"></i> <?= count($article->getComments()) ?></span>

                    <span><i class="fas fa-tags"></i> <?php
                        $tags = $article->getTags();
                        foreach ($tags as $tag) {
                            echo sprintf('<a href="%s">%s</a>', HOMEPAGE . "/tag/" . $tag->getId(), $tag->getName());
                        }
                        ?></span>
                </div>
            </div>   
        </div>      
    </section>
</div>

<div class="col-md-12">
    <?php
    if (count($comments) == 0) {
        echo '<div class="box">Zatiaľ žiadne komentáre</box>';
    }
    ?>

    <?php
    if (count($comments) >= 0) {
        foreach ($comments as $comment) {
            ?>
            <div class="box">
                <div>
                    <small><?= $comment->getName() ?></small>
                </div>
                <div>
                    <small><?= $comment->getTime()->format('d/m/Y H:i') ?></small>
                </div>
                <div>
        <?= $comment->getText() ?>
                </div> 
            </div>



        <?php
        }
    }
    ?>





    <div class="box">



        <form action="../processComment" method="post">
            <div class="form-group">
                <label for="name">Meno:</label>
                <input class="form-control" type="text" name="name">
            </div>
            <div class="form-group">
                <label for="text">Text:</label>
                <textarea class="form-control" type="text" name="text"></textarea>
            </div>
            <div class="form-group">
                <input type="hidden" name="article" value="<?= $article->getId() ?>">
                <button class="btn btn-primary" type="submit">Odoslať</button>
            </div>
        </form>
    </div>
</div>
</div>


<?php require 'views/include/footer.php' ?>