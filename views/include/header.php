<html lang="sk">
    <head>
	<meta charset="utf-8">
	<title><?= isset($title) ? $title : ''?> :: Article System</title>
        <link href="<?= HOMEPAGE?>/public/css/bootstrap.min.css" rel="stylesheet">
    	<link href="<?= HOMEPAGE?>/public/css/style.css" rel="stylesheet">
        <link href="<?= HOMEPAGE?>/public/fontawesome/css/all.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Itim&display=swap" rel="stylesheet"> 
    </head>
    <body>
        <div id="wrapper">
            <div id="header">
                <header>
                    <div class="container">
                        <?php require 'views/include/menu.php'; ?>
                        <h1>Article System</h1>
                    </div>
                </header>
            </div>
            <div id="content">
                <div class="container">