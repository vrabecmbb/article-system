<?php require 'views/include/header.php' ?>

<div class="col-md-12">
    <h1><i class="fas fa-user"></i> <?= $user->getLogin() ?></h1>

    <section id="articles">
        <?php
        if (count($articles) == 0) {
            echo 'Žiadne články';
        } else {

            foreach ($articles as $article) {
                ?>
                <div class="item">
                    <div class="head">
                        <h2><a href="<?= HOMEPAGE ?>/article/view/<?= $article->getId() ?>"><?= $article->getTitle() ?></a></h2>
                        <div class="article_info">
                            <span><?= $article->getPublicationDate()->format('d/m/Y') ?></span>
                            <span><i class="far fa-user"></i> <a href="<?= HOMEPAGE . '/articles/user/' . $article->getUser()->getId() ?>"><?= $article->getUser()->getLogin() ?></a></span>
                        </div>  
                    </div>
                    <div class="content">
                        <img class="float-left" src="<?= HOMEPAGE . "/" . $article->getImage() ?>" />
                        <p><?= $article->getPerex() ?></p>
                    </div>
                    <div style="clear: both"></div>
                    <div class="footer">
                        <div class="article_info">
                            <span><i class="far fa-comments"></i> <?= count($article->getComments()) ?></span>

                            <span><i class="fas fa-tags"></i> <?php
                                $tags = $article->getTags();
                                foreach ($tags as $tag) {
                                    echo sprintf('<a href="%s">%s</a>', HOMEPAGE . "/tag/" . $tag->getId(), $tag->getName());
                                }
                                ?></span>
                        </div>
                    </div>        
                </div>
                <?php
            }
        }
        ?>   
    </section>
</div>


<?php require 'views/include/footer.php' ?>