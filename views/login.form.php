<?php require 'views/include/header.php' ?>

<div class="width-a center-block">
    <div class="box">
        <form action="<?= $action ?>" method="POST">
            <h2><?= $formTitle ?></h2>
            <?php
            if (isset($error)) {
                echo sprintf('<div class="alert alert-danger" role="alert">%s</div>', $error);
            }
            ?>
            <div class="form-group">
                <input class="form-control" type="text" name="email" placeholder="Email">
            </div>
            <div class="form-group">
                <input class="form-control" type="password" name="password" placeholder="Heslo">
            </div>
            <div class="form-group">
                <button class="btn btn-primary btn-block" type="submit"><?= $submitValue ?></button>
            </div>
        </form>    
    </div>
</div>
<?php require 'views/include/footer.php' ?>