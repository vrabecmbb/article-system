<?php

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use App;

require_once "vendor/autoload.php";
require_once "config.php";
require_once "router.php";

// Create a simple "default" Doctrine ORM configuration for Annotations
$isDevMode = true;
$config = Setup::createAnnotationMetadataConfiguration(array(__DIR__."/models/entities"), $isDevMode, "data/DoctrineProxy", NULL, FALSE);
// or if you prefer yaml or XML
//$config = Setup::createXMLMetadataConfiguration(array(__DIR__."/config/xml"), $isDevMode);
//$config = Setup::createYAMLMetadataConfiguration(array(__DIR__."/config/yaml"), $isDevMode);

// database configuration parameters
$conn = $database;

session_start();

// obtaining the entity manager
App::bind('em', EntityManager::create($conn,$config));
$entityManager = App::get('em');

